
Write notes as I work to answer the following quesitons:

* What was your methodology in assessing the data?
1) Review the data for potential modeling issues.
    a) Outliers
    b) Missing Values
        i) 15 records are missing lat/long
        ii) 1 is missing yearbuilt
        iii) 25% has no priorSale
    c) "Out of Sample" problems for prediction (e.g.- a zipcode in pred data doesn't exist in training)

Given issues 1b & 1c, we'll need a method for imputing missing values for each feature we use in the model. This
will likely be an incomplete solution for 1c, however.


* How did you decide which features were important and which ones were of no value?
* Did you consider any strategies other than the one you used for your final model?  What were they?
* How did you train your model?
* How did you evaluate the accuracy of your model?


* Model Prediction Instructions:
    1) Clone Repo: `git clone git@gitlab.com:jacobnaylor/HomeValues.git`
    2) `cd HomeValues`
    3) Create Virtual Environment: "python3 -m venv venv"
    4) `source venv/bin/activate`
    5) Run `pip install -r requirements.txt`
    6) Run python model_scoring --data_path <path_to_test_data>
