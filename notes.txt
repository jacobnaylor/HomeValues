
TODO LIST:
-- Test Deployment (Follow my own usage instructions, with a mock up test dataset.)

-- Move All Data Prep Steps to a Utils function to be shared by main.py & model_scoring.py

-- Use Lat/Long to find Avg SalePrice??/SqFt for N nearest neighbor houses
-- Do a quick analysis of residuals. Most error is likely coming from outlier addresses. However, dropping them
    seems like a bad idea given that they are the most important ones in the dataset.

-- Build Feature for Avg Last Sale Price / SqFt in Zipcode

-- Days since last sale is being imputed as mean. This isn't ideal.
-- Write Some Unit Tests
-- Reinstall Tensorflow w/ CPU Optimization?
-- Map the data?

1) Macroeconomic sale prices:
    a) Use complete dataset to calculate an avg sale price metric.
    b) Same as above, but use $/sqft.
    c) Same as above, but subset to some local region.


Finished List:
-- Build SQLite DB to Hold the Dataset  (May not use this ...)
-- Install Ipykernel, and setup Jupyter for Python 3
-- Do some EDA on the dataset (for insights & appendix of report)
-- Bedrooms / SqFt
-- Bathrooms / SqFt
-- LotSize / SqFt
-- Build Utils for Tracking Performance of Each Model Version
-- Add Neural Net Model
-- Build Linear Regression Benchmark
-- Add Imputation to Pipeline
-- Add Features for "lastSalePrice" & "priorSalePrice"
-- Add Benchmark for "Predict All Zeros"
-- Add Benchmark for "Predict Mean estimated_value"
-- Add OneHotEncoding for Categorical Features
-- Add ZipCode Feature
-- Add Days since last sale feature
-- Add Days since prior sale feature
-- Add bed/bath ratio feature
-- Write Model Pickling
-- Write Model Scoring Script
