import os, sys, math, argparse, pickle
import pandas as pd

from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

from utils.clean_data import clean_data
from utils.features import build_features

# TODO: This has become a bit of spaghetti code.  On refactor, need to reign this in & modularize things a bit more.

rel_path = os.path.dirname(os.path.abspath(__file__))
default_data_path = os.path.join(rel_path, "data/", "scoring_data.csv")

parser = argparse.ArgumentParser("model_scoring")
parser.add_argument("--data_path", help="Complete file path to home value data to be scored", type=str
                    , required=False, default=default_data_path)
args = parser.parse_args()

if os.path.isfile(args.data_path):
    print("Using this path for scoring data:", args.data_path)
else:
    err_msg = "No file at: "+args.data_path+"\nEither move scoring file there, or provide command line argument specifying file path."
    raise ValueError(err_msg)

model_to_use = "RF"

# Read in Data
data = pd.read_csv(args.data_path, header=0)

# Load Model
model_filename = "trained_" + model_to_use + ".pkl"
model_path = os.path.join(rel_path, "saved_models/", model_filename)

regressor = joblib.load(model_path)

# Prep Data for Scoring
data = clean_data(data)

# Categorical Encoding
categorical_data = pd.get_dummies(data['zipcode'], prefix="zip").fillna(0)
categorical_features = list(categorical_data.columns.values)
data = pd.concat([data, categorical_data], axis=1, sort=True)
data['lastSaleDate'] = pd.to_datetime(data['lastSaleDate'])
data['priorSaleDate'] = pd.to_datetime(data['priorSaleDate'])
max_date = data['lastSaleDate'].max()

# Define some column names
target = 'estimated_value'
raw_cols_to_keep = ['estimated_value', 'bedrooms', 'bathrooms', 'rooms', 'squareFootage', 'lotSize'
    , 'yearBuilt', 'lastSaleAmount', 'priorSaleAmount']
constructor_cols = ['lastSaleDate']

cols_to_keep = raw_cols_to_keep + categorical_features

# Subset to Modeling Data & Build New Features
data[raw_cols_to_keep] = data[raw_cols_to_keep].astype(float)
data = build_features(data, cols_to_keep, max_date)

all_names = data.columns
feature_names = [c for c in all_names if c not in ['id', 'estimated_value']]
numeric_features = [c for c in feature_names if c not in categorical_features]

# Resolve Missing/Extra Columns in Test Data

# Load all columns present in training data
column_names_path = os.path.join(rel_path, "saved_models/", "training_column_names.pkl")
with open(column_names_path, 'rb') as f:
    training_names = pickle.load(f)

# Get columns missing in the test data
missing_cols = set(training_names) - set(all_names)

# Add a missing column in test set with default value equal to 0
for c in missing_cols:
    data[c] = 0

resolved_column_names = [c for c in training_names if c not in ['id', 'estimated_value']]

# Ensure the order of column in the test set is in the same order than in train set
data = data[training_names]

# Split data for testing. Will run preds using X_vals, and calculate RMSE against y_true
X_vals, _, y_true, _ = train_test_split(data[resolved_column_names], data[target], test_size=0, random_state=42, shuffle=True)

# Run prediction
y_preds = regressor.predict(X_vals)

# Save preds to file
preds_output = os.path.join(rel_path, "data/", "test_predictions.csv")
predictions_df = pd.DataFrame({"y_preds": y_preds, "y_true": y_true})
predictions_df['resids'] = predictions_df['y_true'] - predictions_df['y_preds']
predictions_df.to_csv(preds_output, index=False)
# print(predictions_df.sort_values("resids", ascending=False).head(20))

# Calculate RMSE
rmse = round(math.sqrt(mean_squared_error(y_true=y_true, y_pred=y_preds)), 0)
print("Prediction Error on Test Dataset is:", rmse)
print("Predictions saved to file:", preds_output)
