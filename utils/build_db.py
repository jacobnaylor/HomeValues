import os, csv, sqlite3

rel_path = os.path.abspath(os.path.dirname(__file__))


def open_db_conn(db_path=os.path.join(rel_path, "../../data/home-values.db")):
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()
    return connection, cursor


def rebuild_db(raw_data_path=os.path.join(rel_path, "../../data/single_family_home_values.csv")):
    conn, c = open_db_conn()

    c.execute('''CREATE TABLE home_values
                 (id int, address text, city text, state real, zipcode int, latitude real, longitude real
                 , bedrooms int, bathrooms int, rooms int, squareFootage int, lotSize int, yearBuilt int
                 , lastSaleDate text, lastSaleAmount int, priorSaleDate text, priorSaleAmount int
                 , estimated_value int)''')

    with open(raw_data_path, 'rb') as f:
        # First line in file for column headings by default
        reader = csv.DictReader(f)
        to_db = [(row['id'], row['address'], row['city'], row['state'], row['zipcode'], row['latitude']
                  , row['longitude'], row['bedrooms'], row['bathrooms'], row['rooms'], row['squareFootage']
                  , row['lotSize'], row['yearBuilt'], row['lastSaleDate'], row['lastSaleAmount']
                  , row['priorSaleDate'], row['priorSaleAmount'], row['estimated_value']
                  ) for row in reader]

    c.executemany("""INSERT INTO home_values (id, address, city, state, zipcode, latitude, longitude, bedrooms
                                            , bathrooms, rooms, squareFootage, lotSize, yearBuilt
                                            , lastSaleDate, lastSaleAmount, priorSaleDate, priorSaleAmount
                                            , estimated_value) VALUES (?, ?);""", to_db)
    conn.commit()
    conn.close()
