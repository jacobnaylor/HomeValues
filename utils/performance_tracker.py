import os, math
import pandas as pd
from pandas.errors import EmptyDataError
from sklearn.metrics import mean_squared_error

rel_path = os.path.dirname(os.path.abspath(__file__))
performance_log_path = os.path.join(rel_path, "../", "data/", "performance_log.csv")


def update_model_performance(version_num, version_name, model_name, rmse, notes, overwrite_run=False):
    """
    Update a csv file containing model performance for each model type run at each version number. The idea here is
    to track the performance of each kind of model as I improve the data preprocessing, model features, and implement
    new model implementations.
    :param version_num: An Int denoting which pipeline iteration this is. Basically this just increments with new versions.
    :param version_name: A Str denoting the pipeline version.  E.G. 'v0.5'
    :param model_name: Name of the model type used from the "models.py" file.  E.G. "RF" for RandomForest
    :param notes: A str containing notes of what changed in this version_num.
    :param rmse: The Root Mean Squared Error of the model.  (model performance)
    :param overwrite_run: A flag to determine whether to overwrite a record if it already exists.
    :return: None: Todo return metrics on new record & best record?
    """

    # Read in Perf Log
    try:
        perf_df = pd.read_csv(performance_log_path, sep="|", header=0)
    except EmptyDataError:
        perf_df = pd.DataFrame(columns=["version_num", "version_name", "model_name", "rmse", "notes"])

    # Create 1 Row DF with new record
    new_record = pd.DataFrame({'version_num': [version_num],
                               'version_name': [version_name],
                               'model_name': [model_name],
                               'rmse': [rmse],
                               'notes': [notes]})

    # Check if Current Version Was Previously Run
    if ((perf_df['version_num'] == version_num) & (perf_df['model_name'] == model_name)).any():
        if overwrite_run:
            # Overwrite the Row
            keep_records = perf_df[~((perf_df['version_num'] == version_num) & (perf_df['model_name'] == model_name))]
            new_df = keep_records.append(new_record)

        else:
            # Do Nothing
            return None
    else:
        # Write new row
        new_df = perf_df.append(new_record)

    new_df.to_csv(performance_log_path, sep="|", index=False, mode='w')
    return None


def parse_grid_search_results(search):
    search_results = {}
    for n, param in enumerate(search.cv_results_['params']):
        search_results[str(param)] = math.sqrt(abs(search.cv_results_['mean_test_score'][n]))

    sorted_results = [(key, search_results[key]) for key in
                      sorted(search_results, key=search_results.get, reverse=False)]
    return sorted_results


def run_benchmarks(benchmarks, data, version_num, version_name, notes, overwrite_run=False):
    for benchmark in benchmarks:
        model_name = "benchmark_"+benchmark
        rmse = fixed_forecast_error(data, benchmark)
        update_model_performance(version_num, version_name, model_name, rmse, notes, overwrite_run=overwrite_run)
    return None


def fixed_forecast_error(df, method):
    df = df[['estimated_value', 'lastSaleAmount']]

    if method == 'zero':
        df['pred'] = 0
    elif method == 'mean':
        mean = df['estimated_value'].mean()
        df['pred'] = mean
    else:
        raise ValueError("Invalid benchmark method: "+str(method))

    rmse = math.sqrt(mean_squared_error(df['estimated_value'], df['pred']))

    return rmse
