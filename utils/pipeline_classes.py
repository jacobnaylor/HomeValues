from sklearn.base import BaseEstimator, TransformerMixin


class ColumnSelection(BaseEstimator, TransformerMixin):
    """
    Transformer to select a set of columns for modeling
    """
    def __init__(self, cols):
        self.cols = cols

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return X[[self.cols]]
