import pandas as pd, numpy as np


def clean_data(data):
    """
    Apply some hard-coded data cleaning to remove outliers & clearly bad data.
    :param data: A pandas dataframe containing the raw data.
    :return: A pandas DF with bad data removed or zeroed out.
    """

    # Wipe out bad data in "lastSaleAmount" column
    data.loc[data['priorSaleAmount'] > 7000000, 'priorSaleAmount'] = np.nan
    data.loc[data['lastSaleAmount'] > 7000000, 'lastSaleAmount'] = np.nan

    return data
