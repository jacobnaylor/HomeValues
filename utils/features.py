import pandas as pd


def build_features(data, raw_cols_to_keep, max_date):
    """
    Construct modeling features for HomeValues model.
    :param data: A pandas dataframe containing the raw data.
    :param numeric_features: Number of features being fed into model.
    :return: A pandas DF containing all model features and the target column.
    """

    data['beds_per_sqft'] = data['bedrooms'] / data['squareFootage']
    data['baths_per_sqft'] = data['bathrooms'] / data['squareFootage']
    data['lot_per_sqft'] = data['lotSize'] / data['squareFootage']
    data['beds_per_bath'] = data['bedrooms'] / data['bathrooms'].apply(lambda x: max(x, 1))

    data['daysSinceLastSale'] = (max_date - data['lastSaleDate']).apply(lambda x: x.days)
    data['daysSincePriorSale'] = (max_date - data['priorSaleDate']).apply(lambda x: x.days)

    new_cols = ['beds_per_sqft', 'baths_per_sqft', 'lot_per_sqft', 'beds_per_bath', 'daysSinceLastSale', 'daysSincePriorSale']
    keep_cols = raw_cols_to_keep + new_cols

    return data[keep_cols]
