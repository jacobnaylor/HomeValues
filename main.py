import math, os, sys, pickle
import pandas as pd, numpy as np

from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.impute import SimpleImputer
from sklearn.externals import joblib

# from utils.pipeline_classes import ColumnSelection
from utils.features import build_features
from utils.clean_data import clean_data
from utils.performance_tracker import parse_grid_search_results, update_model_performance, run_benchmarks
from models import pull_model

# Disable annoying warnings
pd.options.mode.chained_assignment = None

# Model Details for Performance Tracking
version_num = 9
version_name = 'v0.9'
model_names = ['LR', 'NN', 'RF', 'GBR']
# model_names = ['RF']
notes = "Cleaned bad data from lastSaleAmount & priorSaleAmount features."
benchmarks = ["zero", "mean"]

# Read In Data
rel_path = os.path.dirname(os.path.abspath(__file__))
raw_data_path = os.path.join(rel_path, "data/", "single_family_home_values.csv")
data = pd.read_csv(raw_data_path, header=0)

# Run Benchmark Predictions for Comparison Purposes
run_benchmarks(benchmarks, data.copy(deep=True), version_num, version_name, notes, overwrite_run=True)

# Clean Data
data = clean_data(data)

# Categorical Encoding
categorical_data = pd.get_dummies(data['zipcode'], prefix="zip").fillna(0)
categorical_features = list(categorical_data.columns.values)
data = pd.concat([data, categorical_data], axis=1, sort=True)
data['lastSaleDate'] = pd.to_datetime(data['lastSaleDate'])
data['priorSaleDate'] = pd.to_datetime(data['priorSaleDate'])
max_date = data['lastSaleDate'].max()

# Define some column names
target = 'estimated_value'
raw_cols_to_keep = ['estimated_value', 'bedrooms', 'bathrooms', 'rooms', 'squareFootage', 'lotSize'
    , 'yearBuilt', 'lastSaleAmount', 'priorSaleAmount']
constructor_cols = ['lastSaleDate']

cols_to_keep = raw_cols_to_keep + categorical_features

# Subset to Modeling Data & Build New Features
data[raw_cols_to_keep] = data[raw_cols_to_keep].astype(float)
data = build_features(data, cols_to_keep, max_date)

all_names = data.columns
feature_names = [c for c in all_names if c not in ['id', 'estimated_value']]
numeric_features = [c for c in feature_names if c not in categorical_features]

# Save off all columns present in training data
column_names_path = os.path.join(rel_path, "saved_models/", "training_column_names.pkl")
with open(column_names_path, 'wb') as f:
    pickle.dump(list(all_names), f)

# Split data for training
X_train, _, y_train, _ = train_test_split(data[feature_names], data[target], test_size=0, random_state=42, shuffle=True)
number_of_features = len(X_train.columns)

# Define objects to Normalize Data & Impute Missing Values
scaler = preprocessing.StandardScaler()
imputer = SimpleImputer()

# For Each Model to Run, Pull the Object & Params, build Pipeline, do grid search, parse results, & update log
for model_name in model_names:
    print("Running", model_name, "Training")

    # Pull Model Object
    estimator, model_params = pull_model(model_name, number_of_features)
    param_grid = {}
    # Parse model_params and add them to the parameter search
    for param in model_params:
        param_name = 'estimator__' + param
        param_grid[param_name] = model_params[param]

    # Define Steps in the pipeline & build Pipeline object
    steps = [('imputer', imputer)
           , ('scaler', scaler)
           , ('estimator', estimator)]
    pipeline = Pipeline(steps=steps)

    # Define Gridsearch & run it
    search = GridSearchCV(pipeline, param_grid, scoring='neg_mean_squared_error', n_jobs=2, iid=False, cv=5, return_train_score=False)
    search.fit(X_train, y_train)

    # Pull best model object & save it to a file
    regressor = search.best_estimator_
    model_file_name = "trained_"+model_name+".pkl"
    save_model_path = os.path.join(rel_path, "saved_models/", model_file_name)
    joblib.dump(regressor, save_model_path)

    if model_name == "RF":
        feature_importance_path = os.path.join(rel_path, "data/", "feature_importance.csv")
        feature_importance_df = pd.DataFrame(columns=feature_names)
        feature_importance = regressor.named_steps['estimator'].feature_importances_.tolist()
        feature_importance_df = feature_importance_df.append(pd.Series(data=feature_importance, index=feature_names), ignore_index=True)
        feature_importance_df.to_csv(feature_importance_path, sep="|", index=False, mode='w')

    # Print a bunch of results
    best_rmse = math.sqrt(abs(search.best_score_))
    print("MODEL TYPE:", model_name)
    print(best_rmse)
    print(search.best_params_)

    parsed_results = parse_grid_search_results(search)
    for key, value in parsed_results:
        print(key, value)

    # Update model performance log
    update_model_performance(version_num, version_name, model_name, best_rmse, notes, overwrite_run=True)
